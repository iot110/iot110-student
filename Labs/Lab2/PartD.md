[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part D - Lab2 HTML Webpage ###
**Objective:** Now that we have our webserver api and driver architectural
components built and tested it's time to create our IoT website for driving
and providing status for our sensor / actuator system. Remember that this
webpage ```index.html``` has some slight non-HTML properties provided by the
Flask framework for embedded python execution using index.html as a template
(in the templates folder) and embedded code going into ```{{  code }}```
sections of the HTML.

#### Step D1: Create a Flask Templates Folder
Flask has this nice capability to intereact with HTML by placing your HTML code
into a directory right off the *root* webpage app.
```
pi$ cd Labs/Lab2/webapp
pi$ mkdir templates
pi$ cd templates
pi$ touch index.html   # creates a blank index.html to be edited in Atom
```

#### Step D2: Start by creating the basic HTML *standard* structure
A good way to enter HTML code is to use CodePen (codepen.io).  After you have
opened a free account you should prototype your code in CodePen using their
WYSIWYG web page rendering and HTML tidy and syntax checking utilities.  Start
by entering the basic html document structure PLUS a scripting area including
the jQuery *minified* javascript library as shown in the URL.
```html
<!doctype html>
<html>

  <head>
    <title>UW IoT Lab2</title>
  </head>

  <body>

    <!-- ... our HTML goes here!! -->

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>

    <script>
      <!-- ... our custom JS jQuery scripting goes here!! -->
    </script>

  </body>
</html>
```
Select ```Tidy HTML``` and ```Analyze HTML``` to clean up indents and spaces as
well as checking the syntax of your HTML code.  Then select-all and copy the
entire page and paste into your Atom editor (mapped into the RPi).You can go 
back and forth from your CodePen and Atom to copy-paste updates into the 
index.html located in the ```templates``` directory.

#### Step D3: Add python embedded script status structure
The state of the switch and LEDs will be read each time the web page loads if
we can embed a bit of Python into the HTML <body> code using {{ code }} methods.
Prototype this all in CodePen initially (and don't forget to rename and save
your *pen*).

```html
<h2>UW IoT | Lab 2: GPIO API and WebApp</h2>
<h3>Switch: {{'On' if switch else 'Off'}}</h3>
<h4>RED LED (on GPIO18): {{'ON' if led1 else 'OFF'}}</h4>
<h4>GREEN LED (on GPIO13): {{'ON' if led2 else 'OFF'}}</h4>
<h4>BLUE LED (on GPIO23): {{'ON' if led3 else 'OFF'}}</h4>
```
Refresh the webpage when pressing and releasing the SWITCH to see if the webpage
variables update as we expect.  You might try a few CURLs to manually change the
LED state and perform a similar reload to see if the LED states are picked up.

#### Step D4: Add html buttons (one for each LED)
Each HTML button will toggle the state of the LED if we just add an *id* to the
button so that the jQuery script code (below) will "find" that button by name.
```html
<p>
  <button type='button' id='red_led_btn'>RED LED</button><br><br>
  <button type='button' id='grn_led_btn'>GREEN LED</button><br><br>
  <button type='button' id='blu_led_btn'>BLUE LED</button><br><br>
</p>
```
Let's also add a little style to the buttons to give them some color corresponding
to the LED they control. Add the following ```style``` attribute to each of the
buttons and then change the background color to the corresponding LED color.
```html
  <button style="width:100px; height:50px; color:white; background:red" ... >
  <button style= ... >
  <button style= ... >
```
OK!  Done with the main HTML body code.  That's it for now in this section.

[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
