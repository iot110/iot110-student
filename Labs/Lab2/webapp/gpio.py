import RPi.GPIO as GPIO

LED1_PIN = 18
LED2_PIN = 23
SW_PIN = 27

class IoTGPIO(object):
	
	def __init__(self):
 		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(LED1_PIN, GPIO.OUT)
		GPIO.setup(LED2_PIN, GPIO.OUT)
		GPIO.setup(SW_PIN, GPIO.IN)
		GPIO.setup(SW_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

	def read_switch(self):
		switch = GPIO.input(SW_PIN)
		if (switch == 0):
			switch = 1
		else:
			switch = 0
		return switch

	def set_led(self, led, value):
		tmp_led = 0
		if (led == 1):
			tmp_led = LED1_PIN
		if (led == 2):
			tmp_led = LED2_PIN

		GPIO.output(tmp_led, value)

	def get_led(self, led):
		tmp_led = 0
		if (led == 1):
			tmp_led = LED1_PIN
		if (led == 2):
			tmp_led = LED2_PIN

		return GPIO.input(tmp_led)

