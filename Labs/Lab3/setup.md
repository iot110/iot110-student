[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab3

### Objectives
For this lab we will continue building on our IoT webapp by adding *Advanced*
GPIO capability to get us started using *Server Sent Events* in Python/Flask.  We
will also spruce up our web page a bit using the Twitter *bootstrap* web
framework and the adjust the size and spacing to fit on the RPI just in case
you want to see it stand alone on the RPi's smaller own web browser screen.

The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartA.md) Add Twitter Bootstrap Framework.
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartB.md) Add additional
capbility to main.py for *Server Sent Events*.
* [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartC.md) Organize and Code Web Assets.
* [PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartD.md) Add Bootstrap UI Elements and CSS Styles.
* [PART E](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartE.md) Digital Debounce.

[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
