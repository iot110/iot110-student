[LAB7 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab7/setup.md)

## Part A - Check Starter Files and Sensors
**Synopsis:** In this part, we will copy the previous lab files and re-test that
the BMP280 sensor is still operating correctly.

## Objectives
* Copy the lab 5 files to this lab dir
* Run the GPIO test
* Run the BMP280 temperature and pressure sensor test

### Step A1: Copy Lab Starter Files
```sh

pi$ cd <lab 7 directory>
pi$ cp <lab 5 dir>/webapp .
pi$ cd webapp
pi$ tree  .
.
├── bmp280.py
├── bmp280.pyc
├── debouncer.py
├── debouncer.pyc
├── debouncer_test.py
├── gpio.py
├── gpio.pyc
├── gpio_test.py
├── main.py
├── static
│   ├── css
│   │   ├── bootstrap.min.css
│   │   ├── jquery-ui.min.css
│   │   ├── jquery-ui.theme.min.css
│   │   ├── lab5.css
│   │   └── morris.css
│   ├── icon
│   │   └── favicon.ico
│   └── js
│       ├── bootstrap.min.js
│       ├── jquery-3.1.1.min.js
│       ├── jquery-ui.min.js
│       ├── lab5.js
│       ├── morris.min.js
│       └── raphael.min.js
└── templates
    └── index.html

```    

### Step A2: Run GPIO Unit Test

The following GPIO test python code is from gpio_test.py.  Run the code to verify
the breadboard LED and switch wiring is correct and working.

```python
#!/usr/bin/python
import time
from gpio import PiGpio
piio = PiGpio()

print("Blinking LEDs and checking switch (^C to stop)\n")
while True:
    print "LED: ",
    for ledID in range(1, 4):
        print "{0}".format(ledID),
        piio.set_led(ledID, True)
        time.sleep(0.3)
        piio.set_led(ledID, False)
        time.sleep(0.3)
        piio.toggle_led(ledID)
        time.sleep(0.1)
        piio.toggle_led(ledID)
        time.sleep(0.1)
    print(" Sw: {0}".format(piio.get_switch()))
    time.sleep(0.2)
```

```sh
pi$ python gpio_test.py
Blinking LEDs and checking switch (^C to stop)

LED:  1 2 3  Sw: False
LED:  1 2 3  Sw: True
```

### Step A3: Run BMP280 Unit Test

The following BMP280 test function is out of the copied BMP280.py file.  Run the test function
to verify the breadboard wiring is still correct and working.

```python
# =============================================================================
# main to test from CLI
def main():

    # create an instance of my pi bmp280 sensor object
    pi_bmp280 = PiBMP280()

    # Read the Sensor ID.
    (chip_id, chip_version) = pi_bmp280.readBMP280ID()
    print "    Chip ID :", chip_id
    print "    Version :", chip_version

    # Read the Sensor Temp/Pressure values.
    (temperature, pressure) = pi_bmp280.readBMP280All()
    print "Temperature :", temperature, "C"
    print "   Pressure :", pressure, "hPa"

if __name__=="__main__":
   main()
# =============================================================================
```

```sh
pi$ ./bmp280.py
# -----------------------------------------------------------------------------
    Chip ID : 88
    Version : 1
Temperature : 24.78 C
   Pressure : 1006.53054419 hPa
# -----------------------------------------------------------------------------
```

[PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab7/PartB.md) Develop pwm.py driver and unit test

[LAB7 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab7/setup.md)
