[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)

## Part A - Install MQTT
**Synopsis:** In this part, we will install and test MQTT.

## Objectives
* apt-get MQTT libraries
* Configure Mosquitto server
* Configure and establish user and password

### Step A1: Install Libraries
```sh
sudo apt-get install mosquitto mosquitto-clients python-mosquitto
sudo pip install paho-mqtt

```    

### Step A2: Configure Mosquitto  (/etc/mosquitto/conf.d/mosquitto.conf)
```
pi$ sudo vim /etc/mosquitto/conf.d/mosquitto.conf

# Config file for mosquitto
#
# See mosquitto.conf(5) for more information.

# user mosquitto
# max_queued_messages 200
# message_size_limit 0
# allow_zero_length_clientid true
# allow_duplicate_messages false

listener 1883
# autosave_interval 900
# autosave_on_changes false
# persistence true
# persistence_file mosquitto.db
allow_anonymous false
password_file /etc/mosquitto/passwd
```

### Step A3: Setup Username ("pi") and Password ("raspberry")

```sh
pi$ sudo mosquitto_passwd -c /etc/mosquitto/passwd pi
Password: ********* (<= "raspberry")
Reenter password: ********* (<= "raspberry")
```

### Step A4: Start (Restart) the Mosquitto Daemon

```sh
pi$ sudo systemctl restart mosquitto
pi$ sudo systemctl enable mosquitto
```

### Step A5: Subscribe to Topic "iot/test"

```sh
pi$ mosquitto_sub -d -u pi -P raspberry -t iot/test

```

### Step A6: Publish Hello String to Topic "iot/test"

```sh
pi$ mosquitto_pub -d -u pi -P raspberry -t iot/test -m "Hello MQTT!"

```

[PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartB.md)  Install MQTT Client Tool MQTT.fx

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)
