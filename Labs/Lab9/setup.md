[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab9

**Synopsis:** For this lab we will extend the introduction to MQTT and IoT edge
devices by connecting to the UW/instructor provisioned Microsoft Azure cloud
system and the Azure IoT Suite.  We will perform manual connections to an MQTT
Broker on the Azure cloud, provision security certificates, and connect to the Azure
IoT Hub.

### Objectives

* Install MQTT Message Broker on an Azure Ubuntu VM
* Connect our IoT Gateway code to Azure IoT Hub

The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/PartA.md) Install and test MQTT Message Broker on an Azure Ubuntu VM
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/PartB.md) Explore Azure IoT Hub

[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
