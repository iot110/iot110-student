#### Course Labs

| Lab  | Topics  |
|---|---|
| 1  | Setup RPi, Hello World Python script  |
| 2  | I/O LEDs and Switch with WebApp  |
| 3  | Expand WebApp using SSE  |
| 4  | Connect BMP280: I2C Pressure/Temp Sensor |
| 5  | Add BMP280 Data to WebApp, add charting |
| 6  | Connect SenseHat and plug into WebApp |
| 7  | Add stepper motor to breadboard WebApp |
| 8  | Current: Connect to local MQTT Broker |
| 9  | Current: Add Cloud MQTT Broker |
| 10 | Class Hackathon |
