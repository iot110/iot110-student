# Quick Help

### Class Quick Reference

* Class Wireless:
> SSID UW-IoT110-R  
> PSK piIoT110
* http://canvas.uw.edu -- UW Class Website
* https://gitlab.com/iot110/iot110-student
 -- IoT 110 Class GitLab

### Common Issues
 * Use Ctl-Refresh (Ctl-F5 on some systems).
 > Refresh will re-get the current webpage, however it does not always re-get any linked static resources.  If any changes are made to images, css, or javascript files, they may not be loaded.  Control-Refresh will inform the browser to re-get all the webpage resources including the static linked.
 * Use ```<pi_name>``` or ```<pi_name>.local``` or install Samba (mostly for Windows)
 > If there are issues connecting to the Raspberry Pi over the network by name, first try using ```<pi_name>.local```.  If there are still issues finding the RPi from a Windows computer, using Samba will sometimes make the name search more reliable.
